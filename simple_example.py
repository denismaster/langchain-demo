from dotenv import load_dotenv

load_dotenv(".env")

import os
from langchain_core.output_parsers import StrOutputParser
from langchain.prompts import PromptTemplate
from langchain_openai import ChatOpenAI

llm = ChatOpenAI(
    api_key=os.environ.get("OPENAI_API_KEY"),
    base_url=os.environ.get("OPENAI_BASE_URL"),
    model="gpt-3.5-turbo",
    temperature=0.5,
)


prompt_template = """
You need to translate provided user query 
fron russian language to english, italian and spanish.

Query: {input}
"""

prompt = PromptTemplate.from_template(prompt_template)

chain = (
    prompt
    | llm
    | StrOutputParser()
)

response = chain.invoke(input("Введите слово для перевода:\n"))

print(f"Результат нейросети:\n{response}")