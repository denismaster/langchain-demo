# langchain-demo

## Setup
1. Install Python and clone repo
2. Create and activate virtual environment
3. Run `pip install -r requirements.txt`

## .env template
```
OPENAI_API_KEY="..."
OPENAI_BASE_URL="..."
OPENAI_EMBEDDINGS_MODEL="text-embedding-3-small"
OPENAI_LLM_MODEL="gpt-3.5-turbo"

QDRANT_URL="http://localhost:6333"
QDRANT_API_KEY=""
QDRANT_COLLECTION_NAME="test"
```

## Simple example

```bash
python ./simple_example.py
```

## FastAPI Example

```bash
fastapi dev .\api.py
```