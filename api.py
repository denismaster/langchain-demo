from dotenv import load_dotenv

load_dotenv(".env")

from fastapi import FastAPI
from pydantic import BaseModel, Field
from pydantic_settings import BaseSettings

from langchain_core.runnables import RunnablePassthrough
from langchain_core.output_parsers import StrOutputParser

from langchain.prompts import PromptTemplate

from langchain_openai import ChatOpenAI, OpenAIEmbeddings
from langchain_qdrant import Qdrant


class OpenAISettings(BaseSettings):
    base_url: str = Field(validation_alias="OPENAI_BASE_URL")
    api_key: str = Field(validation_alias="OPENAI_API_KEY")
    llm_model: str = Field(validation_alias="OPENAI_LLM_MODEL")
    embeddings_model: str = Field(validation_alias="OPENAI_EMBEDDINGS_MODEL")


class QdrantSettings(BaseSettings):
    url: str = Field(validation_alias="QDRANT_URL")
    api_key: str = Field(validation_alias="QDRANT_API_KEY")
    collection_name: str = Field(
        default="default",
        validation_alias="QDRANT_COLLECTION_NAME",
    )


openai_settings = OpenAISettings()
qdrant_settings = QdrantSettings()

embeddings = OpenAIEmbeddings(
    api_key=openai_settings.api_key,
    base_url=openai_settings.base_url,
    model=openai_settings.embeddings_model,
)

llm = ChatOpenAI(
    api_key=openai_settings.api_key,
    base_url=openai_settings.base_url,
    model=openai_settings.llm_model,
    temperature=0,
)

qdrant = Qdrant.from_existing_collection(
    embedding=embeddings,
    collection_name=qdrant_settings.collection_name,
    url=qdrant_settings.url,
    api_key=qdrant_settings.api_key,
    path=None,
)


app = FastAPI()


class GetJobMarketStateRequest(BaseModel):
    query: str


class GetJobMarketStateResponse(BaseModel):
    response: str


@app.post("/rag")
def rag(
    request: GetJobMarketStateRequest,
) -> GetJobMarketStateResponse:
    prompt_template = """
You are an assitant that can help user to search info about vacancies according to their query below.

You need to provide a description as a summary about the state of the job market using info 
from the vacancies relevant to the query.
Use data about available vacancies only from the context below.

Description in the response should be in russian language.

Context: {context}
Query: {query}
"""
    prompt = PromptTemplate.from_template(prompt_template)

    chain = (
        {
            "context": qdrant.as_retriever(
                search_type="similarity_score_threshold",
                search_kwargs={"score_threshold": 0.3},
            ),
            "query": RunnablePassthrough(),
        }
        | prompt
        | llm
        | StrOutputParser()
    )

    response = chain.invoke(request.query)

    return GetJobMarketStateResponse(
        response=response,
    )
